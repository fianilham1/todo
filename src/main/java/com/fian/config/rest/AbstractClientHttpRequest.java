/*
 * Copyright 2020-2023 (C) Harmonix Teknologi Peentar - All Rights Reserved.
 *
 * THE CONTENTS OF THIS PROJECT ARE PROPRIETARY AND CONFIDENTIAL.
 * UNAUTHORIZED COPYING, TRANSFERRING OR REPRODUCTION OF THE CONTENTS OF THIS PROJECT,
 * VIA ANY MEDIUM IS STRICTLY PROHIBITED.
 *
 * The receipt or possession of the source code and/or any parts thereof does not convey or imply any right to use them
 * for any purpose other than the purpose for which they were provided to you.
 *
 * The software is provided "AS IS", without warranty of any kind, express or implied, including but not limited to
 * the warranties of merchantability, fitness for a particular purpose and non infringement.
 * In no event shall the authors or copyright holders be liable for any claim, damages or other liability,
 * whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software
 * or the use or other dealings in the software.
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 */

package com.fian.config.rest;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

public abstract class AbstractClientHttpRequest {

    protected RestTemplate restTemplate;
    protected HttpHeaders headers = new HttpHeaders();

    protected AbstractClientHttpRequest(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
        this.restTemplate.setRequestFactory(getClientHttpRequestFactory());
        this.restTemplate.setErrorHandler(new ClientErrorHandler());
        this.headers.setContentType(MediaType.APPLICATION_JSON);
        this.headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
    }

    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        int timeout = 60000;
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory =
                new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(timeout);
        clientHttpRequestFactory.setReadTimeout(timeout);
        return clientHttpRequestFactory;
    }


    public HttpHeaders headerWithBearer(String bearer) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + bearer);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }

    public HttpHeaders headerWithBearerWithMultipart(String bearer) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + bearer);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        return headers;
    }

    public <T> ResponseEntity<T>  getResponse(String uri, HttpHeaders headers, Class<T> response) {
        HttpEntity<String> httpEntity = new HttpEntity<>(headers);
        return restTemplate.exchange(uri, HttpMethod.GET,httpEntity, response );
    }

    public <T> ResponseEntity<T> getResponse(String url, HttpHeaders headers, ParameterizedTypeReference<T> response){
        HttpEntity<String> httpEntity = new HttpEntity<>(headers);
        return restTemplate.exchange(url, HttpMethod.GET, httpEntity, response);
    }

    public <T, S> ResponseEntity<T> postResponse(String uri, HttpHeaders headers, S request, Class<T> response) {
        HttpEntity<S> httpEntity = new HttpEntity<>(request, headers);
        return restTemplate.exchange(uri, HttpMethod.POST, httpEntity, response );
    }

    public <T, S> ResponseEntity<T> postResponse(String uri, HttpHeaders headers, S request, ParameterizedTypeReference<T> response) {
        HttpEntity<S> httpEntity = new HttpEntity<>(request, headers);
        return restTemplate.exchange(uri, HttpMethod.POST, httpEntity, response);
    }

    public <T> ResponseEntity<T> postResponse(String uri, HttpHeaders headers, ParameterizedTypeReference<T> response) {
        HttpEntity<String> httpEntity = new HttpEntity<>(headers);
        return restTemplate.exchange(uri, HttpMethod.POST, httpEntity, response );
    }

    public <T> ResponseEntity<T> deleteResponse(String uri, HttpHeaders headers, Class<T> response) {
        HttpEntity<String> httpEntity = new HttpEntity<>(headers);
        return restTemplate.exchange(uri, HttpMethod.DELETE,httpEntity, response );
    }

    public <T> ResponseEntity<T> patchResponse(String uri, HttpHeaders headers, ParameterizedTypeReference<T> response) {
        HttpEntity<String> httpEntity = new HttpEntity<>(headers);
        return restTemplate.exchange(uri, HttpMethod.PATCH, httpEntity, response );
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public HttpHeaders getHeaders() {
        return headers;
    }

}