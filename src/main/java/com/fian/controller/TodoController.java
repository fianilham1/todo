package com.fian.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.fian.exception.ResourceNotFoundException;
import com.fian.model.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fian.config.auth.CurrentUser;
import com.fian.config.auth.UserPrincipal;
import com.fian.service.TodoService;

@RestController
@CrossOrigin(origins = "*")
public class TodoController {

	@Autowired
	private TodoService todoService;

	public TodoController(TodoService todoService) {
		this.todoService = todoService;
	}

	@GetMapping("/users/todos")
	public List<Todo> getTodoListByUserId(@CurrentUser UserPrincipal currentUser) {
		return todoService.todoListByUserId(currentUser.getId());
	}

	@GetMapping("/users/todos/undone")
	public List<Todo> getUnDoneTodoListByUserId(@CurrentUser UserPrincipal currentUser) {
		return todoService.todoListUndoneByUserId(currentUser.getId());
	}

	@GetMapping("users/todos/{id}")
	public ResponseEntity<Todo> getTodoById(@CurrentUser UserPrincipal currentUser, @PathVariable(value = "id") Long todoId)
			throws ResourceNotFoundException {
		Todo todo = todoService.findByIdAndUserId(todoId, currentUser.getId())
				.orElseThrow(() -> new ResourceNotFoundException("Todo not found for this id :: " + todoId));
		return ResponseEntity.ok().body(todo);
	}

	@PostMapping("/users/todos")
	public Todo createTodo(@CurrentUser UserPrincipal currentUser, @Valid @RequestBody Todo todo) {
		todo.setCreatedDate(new Date());
		return todoService.save(currentUser.getId(), todo);
	}

	@PutMapping("/users/todos/{id}")
	public ResponseEntity<Todo> updateTodo(@CurrentUser UserPrincipal currentUser, @PathVariable(value = "id") Long todoId,
			@Valid @RequestBody Todo todoDetails) throws ResourceNotFoundException {
		Todo todo = todoService.findByIdAndUserId(todoId, currentUser.getId())
				.orElseThrow(() -> new ResourceNotFoundException("Todo not found for this id :: " + todoId));

		todo.setDescription(todoDetails.getDescription());
		todo.setDone(todoDetails.isDone());
		todo.setTargetDate(todoDetails.getTargetDate());
		todo.setModifiedDate(new Date());

		final Todo updatedTodo = todoService.save(currentUser.getId(), todo);
		return ResponseEntity.ok(updatedTodo);
	}

	@PutMapping("/users/todos/done/{id}")
	public ResponseEntity<Todo> markTodoDone(@CurrentUser UserPrincipal currentUser, @PathVariable(value = "id") Long todoId) throws ResourceNotFoundException {
		Todo todo = todoService.findByIdAndUserId(todoId, currentUser.getId())
				.orElseThrow(() -> new ResourceNotFoundException("Todo not found for this id :: " + todoId));

		todo.setDone(true);
		final Todo updatedTodo = todoService.save(currentUser.getId(), todo);
		return ResponseEntity.ok(updatedTodo);
	}

	@DeleteMapping("/users/todos/{id}")
	public Map<String, Boolean> deleteTodo(@CurrentUser UserPrincipal currentUser, @PathVariable(value = "id") Long todoId)
			throws ResourceNotFoundException {
		Todo todo = todoService.findByIdAndUserId(todoId, currentUser.getId())
				.orElseThrow(() -> new ResourceNotFoundException("Todo not found for this id :: " + todoId));

		todoService.delete(todo);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
