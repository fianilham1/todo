package com.fian.controller.auth;

import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Collections;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.fian.controller.util.ApiResponse;
import com.fian.service.util.HashSaltPasswordUtil;
import com.fian.controller.util.SignUpRequest;
import com.fian.exception.ResourceNotFoundException;
import com.fian.model.Role;
import com.fian.model.RoleName;
import com.fian.model.Users;
import com.fian.repository.RoleRepository;
import com.fian.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fian.config.auth.JwtTokenRequest;
import com.fian.config.auth.JwtTokenResponse;
import com.fian.config.auth.JwtTokenUtil;
import com.fian.exception.AppException;
import com.fian.exception.AuthenticationException;

@RestController
@CrossOrigin(origins = "*")
public class JwtAuthenticationRestController {

	@Value("${jwt.http.request.header}")
	private String tokenHeader;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	UsersRepository usersRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	private UserDetailsService userService;

	@Autowired
	private HashSaltPasswordUtil hashSaltPasswordUtil;

	@RequestMapping(value = "${jwt.get.token.uri}", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtTokenRequest authenticationRequest)
			throws AuthenticationException, ResourceNotFoundException {

		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final Users user = usersRepository.findByUsername(authenticationRequest.getUsername()).orElseThrow(() -> new ResourceNotFoundException("user is not found"));

		final String token = jwtTokenUtil.generateToken(user.getUsername());

		return ResponseEntity.ok(new JwtTokenResponse(token));
	}

	@RequestMapping(value = "${jwt.refresh.token.uri}", method = RequestMethod.GET)
	public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) throws ResourceNotFoundException {
		String authToken = request.getHeader(tokenHeader);
		final String token = authToken.substring(7);
		String username = jwtTokenUtil.getUsernameFromToken(token);
		final Users user = usersRepository.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("user is not found"));

		if (jwtTokenUtil.canTokenBeRefreshed(token)) {
			String refreshedToken = jwtTokenUtil.refreshToken(token);
			return ResponseEntity.ok(new JwtTokenResponse(refreshedToken));
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@ExceptionHandler({ AuthenticationException.class })
	public ResponseEntity<String> handleAuthenticationException(AuthenticationException e) {
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
	}

	private void authenticate(String username, String password) {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new AuthenticationException("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new AuthenticationException("INVALID_CREDENTIALS", e);
		}
	}

	@PostMapping("${jwt.sign.users.uri}")
	public ResponseEntity<?> registerUser(@RequestBody SignUpRequest signUpRequest) {
		if (usersRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity(new ApiResponse(false, "Username is already taken!"), HttpStatus.BAD_REQUEST);
		}

		if (usersRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"), HttpStatus.BAD_REQUEST);
		}

		// Creating user's account
		Users users = new Users(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
				signUpRequest.getPassword());

		String encryptedPassword;
		try {
			encryptedPassword = hashSaltPasswordUtil.generateStrongPasswordHash(users.getPassword());
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			return new ResponseEntity(new ApiResponse(false, "there is an error in generating password"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		users.setPassword(encryptedPassword);

		Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
				.orElseThrow(() -> new AppException("User Role not set."));

		users.setRoles(Collections.singleton(userRole));

		Users result = usersRepository.save(users);

		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
				.buildAndExpand(result.getUsername()).toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
	}

}
