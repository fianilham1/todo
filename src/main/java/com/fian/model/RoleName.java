package com.fian.model;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}