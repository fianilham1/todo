package com.fian.repository;

import java.util.List;
import java.util.Optional;

import com.fian.model.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long> {

	@Query("select t from Todo t where t.users.id = :userId")
	List<Todo> findAllByUserId(@Param("userId") Long userId);

	@Query("select t from Todo t where t.users.id = :userId and t.done is false")
	List<Todo> findUnDoneByUserId(@Param("userId") Long userId);

	@Query("select t from Todo t where t.id = :id and t.users.id = :userId")
	Optional<Todo> findByIdAndUserId(@Param("id") Long id, @Param("userId") Long userId);
}
