package com.fian.service;

import com.fian.service.util.HashSaltPasswordUtil;
import com.fian.exception.ResourceNotFoundException;
import com.fian.model.Role;
import com.fian.model.Users;
import com.fian.repository.UsersRepository;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class CustomAuthenticationProviderServiceImpl implements AuthenticationProvider {

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    private HashSaltPasswordUtil hashSaltPasswordUtil;

    @SneakyThrows
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken authenticationToken = null;

        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        Users users = usersRepository.findByUsername(username).orElseThrow(()->new ResourceNotFoundException("user is not found"));
        try{
            if (username.equals(users.getUsername()) && hashSaltPasswordUtil.validatePassword(password, users.getPassword())){
                Collection<GrantedAuthority> grantedAuthorities = getGrantedAuthorities(users);
                authenticationToken = new UsernamePasswordAuthenticationToken(
                        new User(username, password, grantedAuthorities),password,grantedAuthorities);
            }else{
                throw new Exception("Password is wrong");
            }
        }catch (Exception ex){
            System.out.println("error authenticate : "+ex);
            throw new Exception("Password is wrong");
        }
        return authenticationToken;
    }

    private Collection<GrantedAuthority> getGrantedAuthorities(Users users){
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (Role role : users.getRoles()) {
            if (role.getName().name().equals("ROLE_ADMIN")) {
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            } else {
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_CUSTOMER"));
            }
        }

        return grantedAuthorities;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}

