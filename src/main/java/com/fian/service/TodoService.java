package com.fian.service;

import java.util.List;
import java.util.Optional;

import com.fian.model.Todo;

public interface TodoService {
	
	List<Todo> todoListByUserId(Long userId);

	List<Todo> todoListUndoneByUserId(Long userId);

	Optional<Todo> findByIdAndUserId(Long todoId, Long userId);

	Todo save(Long userId, Todo todo);

	void delete(Todo todo);

}
