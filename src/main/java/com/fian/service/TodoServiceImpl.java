package com.fian.service;

import com.fian.model.Todo;
import com.fian.model.Users;
import com.fian.repository.TodoRepository;
import com.fian.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TodoServiceImpl implements TodoService {

	@Autowired
	private TodoRepository todoRepository;
	
	@Autowired
	private UsersRepository usersRepository;
	
	@Override
	public List<Todo> todoListByUserId(Long userId) {
		return todoRepository.findAllByUserId(userId);
	}

	@Override
	public List<Todo> todoListUndoneByUserId(Long userId) {
		return todoRepository.findUnDoneByUserId(userId);
	}

	@Override
	public Optional<Todo> findByIdAndUserId(Long todoId, Long userId) {
		return todoRepository.findByIdAndUserId(todoId, userId);
	}

	@Override
	public Todo save(Long userId, Todo todo) {
		Users users = usersRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("User not found with id : " + userId));
		todo.setUser(users);
		
		return todoRepository.save(todo);
	}

	@Override
	public void delete(Todo todo) {
		todoRepository.delete(todo);
	}

}
