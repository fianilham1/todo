create sequence if not exists role_id_seq;

create table if not exists roles
(
    id                   int8 NOT NULL DEFAULT nextval('role_id_seq'::regclass),
    name                 VARCHAR(255) NOT NULL,
    CONSTRAINT role_id_pkey PRIMARY KEY (id)
);

create sequence if not exists user_id_seq;

create table if not exists users
(
    id                    int8 NOT NULL DEFAULT nextval('user_id_seq'::regclass),
    name                 VARCHAR(255) NOT NULL,
    username                 VARCHAR(255) NOT NULL,
    email                 VARCHAR(255) NOT NULL,
    password                 VARCHAR(255) NOT NULL,
    CONSTRAINT user_id_pkey PRIMARY KEY (id),
    CONSTRAINT username_unq UNIQUE (username),
    CONSTRAINT email_unq UNIQUE (email)
);

create table if not exists user_roles
(
    user_id                 int8 NOT NULL,
    role_id                 int8 NOT NULL,
    CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES public.users(id),
    CONSTRAINT fk_role FOREIGN KEY (role_id) REFERENCES public.roles(id),
    CONSTRAINT user_role_unq UNIQUE (user_id, role_id)
);

INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');
INSERT INTO users(name, username, email, password) VALUES('test','test', 'test@test.com', '$2a$10$WlW9TJuHNxH9t4BhrP.zdOLOMrKkJVZNwuoBtqp8mGWuFbsz.7gmm');

INSERT INTO user_roles (user_id, role_id) select u.id, r.id from users u, roles r where u.username = 'test' and r.name = 'ROLE_USER';


create sequence if not exists todo_id_seq;

create table if not exists todo
(
    id                    int8 NOT NULL DEFAULT nextval('todo_id_seq'::regclass),
    description             VARCHAR(255) NOT NULL,
    is_done                 bool NULL DEFAULT false,
    target_date             timestamptz NOT NULL,
    modified_date            timestamptz NULL,
    created_date             timestamptz NULL,
    user_id                 int8 NOT NULL,
    CONSTRAINT fk_user_todo FOREIGN KEY (user_id) REFERENCES public.users(id)
);